const apm = require('elastic-apm-node').start({
  // Override service name from package.json
  // Allowed characters: a-z, A-Z, 0-9, -, _, and space
  serviceName: 'elastic-apm-express-demo-1',

  // Use if APM Server requires a token
  secretToken: '',

  // Set custom APM Server URL (default: http://localhost:8200)
  serverUrl: '',
})


const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => res.send('Hello World!'))
app.get('/authentication', function (req, res) {
  throw new Error('BROKEN') // Express will catch this on its own.
})
app.listen(port, () => console.log(`Example app listening on port ${port}!`))
