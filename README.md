## Demo for running Elastic APM server & Kibana with samle ExpressJs app  
### Cloning this repo  
First, please clone the repo `git clone git@bitbucket.org:t33c33/elastic-apm-express-demo.git`  

### Start the demo  
* Install Docker-Compose.  
* Start APM Server.  
`docker-compose -f apm-server/docker-compose up -d`  
* Start the demo app.  
`npm install elastic-apm-node --save`  
node app.js  

### Generate workload into demo app
* Install load test tool. It can be JMeter, Vegeta, wrk or k6. In this example, let's use vegeta.  
Grab a pre-compiled of vegeta from http://github.com/tsenart/vegeta/releases.  
On MacOS, vegeta could be install using homebrew. Just run `brew install vegeta` on the terminal.  
* Generate workload  
`echo GET localhost:3000/ | vegeta attack -rate 50/1s` // This command will send 50 request per second into the demo app.  
* Check the APM dashboard 
On browser, open `http://localhost:5601`, then select the `APM` feature.  
